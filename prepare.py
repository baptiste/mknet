#!/usr/bin/env python3
from pathlib import Path
from scenarios.fragments import shared
import os, venv

me = Path(os.path.dirname(__file__))
print("--- git submodule ---")
shared.exec("git submodule update --init")

print("--- compile go benchmarks ---")
os.chdir(me / "benchmarks" / "warp")
shared.exec("go build")
os.chdir(me / "benchmarks" / "s3concurrent")
shared.exec("go build")
os.chdir(me / "benchmarks" / "s3lat")
shared.exec("go build")
os.chdir(me / "benchmarks" / "s3ttfb")
shared.exec("go build")
os.chdir(me / "benchmarks" / "s3billion")
shared.exec("go build")
os.chdir(me)

print("--- install python dependencies ---")
venv.create(".venv", with_pip=True)
shared.exec("""
source .venv/bin/activate
which python3
python3 -m pip install .
python3 -m pip install -r scenarios/requirements.txt
""")

print("--- download artifacts ---")
from scenarios.fragments import flavor
flavor.download()

print(f"""done! now, run:
> sudo -i
> cd {os.getcwd()}
> source ./.venv/bin/activate
> ./mknet scenario ./topo/with-vdsl.yml ./scenarios/garage-s3lat garage-v0.8
""")
