# mknet

mknet is a tool to simulate various network topologies
locally thanks to network namespaces and traffic control (tc).

## Prepare your environment

Get the repository:

```bash
git clone https://git.deuxfleurs.fr/Deuxfleurs/mknet.git
cd mknet
```

Run our configuration script:

```bash
./prepare.py
```

Now, you are ready to launch an experiment:

```bash
sudo -i
source .venv/bin/activate
./mknet scenario ./topo/50ms.yml ./scenarios/garage-s3lat garage-v0.8
```

If a script crash, you must manually destroy the topology:

```
./mknet destroy
```

## Topologies

All topologies:
  - `./topo/dc.yml` - A 3 node topology connected with a 1Gbit/s link and a 1ms latency
  - `./topo/with-vdsl.yml` - A topology mixing datacenters with fast internal connectivity and an isolated VDSL node
  - `./topo/50ms.yml` - An artifical topology simulating nodes with high bandwidth but with a fixed 50ms latency, useful to quantify the impact of latency on a distributed software
  - `./topo/multi-dc.yml` - Simulate 3 DC interconnected with 50ms latency WAN network and close to zero latency inside the DC
  - `./topo/{1,5,10,50,100}mbps.yml` - Simulate 3 nodes interconnected with a bandwidth bottleneck.

Feel free to write new topologies!

## Scenarios

All scenarios:
 - `./scenarios/garage-s3lat [garage-v0.7|garage-v0.8]` - Run s3lat on Garage
 - `./scenarios/garage-concurrent [garage-v0.7|garage-v0.8]` - Run s3concurrent on Garage
 - `./scenarios/garage-warp [garage-v0.7|garage-v0.8] [default|fast]` - Run warp on Garage. 2 flavors are available: fast and default.

*Scenarios take optional flavors as input that modulate their behavioir. Passing them is not mandatory,
a default one will be selected for you.*

How to run them:

```bash
./mknet scenario <topology> <scenario> [flavors...]
./mknet scenario ./topo/50ms.yml ./scenarios/garage-s3lat garage-v0.8
```

How to write good scenarios:
  - If a scenario can be run with multiple different parameters, write one scenario with multiple flavors
  - If the logic ran is different, write a new scenario
  - A scenario code must remain short and looks like a DSL, abstract the logic in the `fragments/` module

## Manual usage

```bash
./mknet create ./topo/with-vdsl.yml
./mknet run-all ./scenarios/garage-manual
./mknet run dc1:dc1s1 /tmp/mknet-bin/garage* -c /tmp/mknet-store/garage/dc1/dc1s1/garage.toml status
./mknet run-all ./scenarios/clean
./mknet destroy

